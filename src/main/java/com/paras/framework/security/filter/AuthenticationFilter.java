package com.paras.framework.security.filter;

import java.io.IOException;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;

import com.paras.framework.security.spring.CookieAuthentication;

/**
 * An AuthenticatorFilter.
 * It works with cookies.
 * 
 * It no session is present, it creates a session and an spring authentication object with null principal, and zero authorities.
 * @author Paras
 *
 */
public class AuthenticationFilter implements Filter {
	
	private static Logger LOGGER = Logger.getLogger( AuthenticationFilter.class );
	
	@Override
	public void init( FilterConfig config ) {}
	
	@Override
	public void destroy() {}
	
	@Override
	public void doFilter( ServletRequest req, ServletResponse res, FilterChain chain ) throws IOException, ServletException {
		LOGGER.debug( "In AuthenticationFilter | Starting Execution of doFilter");
		
		HttpServletRequest httpRequest = ( HttpServletRequest ) req;
		
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		HttpSession session = null;
		
		if( auth == null ) {
			LOGGER.debug( "In AuthenticationFiler | In doFiler | No authentication object found. ");
			session = httpRequest.getSession( false );
			
			if( session == null ) {
				LOGGER.debug( "In AuthenticationFilter | In doFilter | Null Session | Creating New Session.");
				session = httpRequest.getSession( true );
			}
			
			LOGGER.debug( "In AuthenticationFilter | In doFilter | Creating Blank Authentication");
			auth = CookieAuthentication.createBlankAuthentication();			
			auth.setAuthenticated( true );
			
			SecurityContextHolder.getContext().setAuthentication( auth );
			
		}
		
		chain.doFilter( req,  res );
		
		LOGGER.debug( "In AuthenticationFilter | Finished Execution of doFilter");
	}
}
