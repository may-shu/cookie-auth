package com.paras.framework.security.spring;

import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.userdetails.UserDetails;

/**
 * CookieAuthentication is a holder holding all debugrmation about logged in user.
 * @author Paras
 *
 */
public class CookieAuthentication implements Authentication {

	/**
	 * 
	 */
	private static final long serialVersionUID = 5324586494444112808L;

	private static Logger LOGGER = Logger.getLogger( CookieAuthentication.class );
	
	CurrentUserDetails details = null;
	private String principal = null;	
	private boolean authenticated = false;
	
	@Override
	public List<RoleAuthority> getAuthorities() {
		
		if( details == null ) {
			return new ArrayList<RoleAuthority>();
		}
		
		LOGGER.debug( "In CookieAuthentication | Starting Execution of getAuthorities ");
		LOGGER.debug("In CookieAuthentication | In getAuthorities | Authorities =" + details.getAuthorities());
		LOGGER.debug( "In CookieAuthentication | Finished Execution of getAuthorities" );
		
		return details.getAuthorities();
	}
	
	@Override
	public String getName() {
		
		if( details == null ) {
			return null;
		}
		
		LOGGER.debug( "In CookieAuthentication | Starting Execution of getName ");
		LOGGER.debug("In CookieAuthentication | In getAuthorities | Name =" + details.getName());
		LOGGER.debug( "In CookieAuthentication | Finished Execution of getName" );
		
		return details.getName();
	}
	
	@Override
    public Object getCredentials() {
		
		if( details == null ) {
			return null;
		}
		
        LOGGER.debug( "In CookieAuthentication | Starting Execution of getCredentials ");
		LOGGER.debug("In CookieAuthentication | In getCredentials | Credentials =" + details.getPassword());
		LOGGER.debug( "In CookieAuthentication | Finished Execution of getCredentials" );
		
		return details.getPassword();
    }
	
	@Override
    public Object getDetails() {
		
		if( details == null ) {
			return null;
		}
		
        LOGGER.debug( "In CookieAuthentication | Starting Execution of getDetails ");
		LOGGER.debug( "In CookieAuthentication | Finished Execution of getDetails" );
        
        return details;
    }
	
	@Override
    public Object getPrincipal() {
		
		if( details == null ) {
			return null;
		}
		
    	LOGGER.debug( "In CookieAuthentication | Starting Execution of getPrincipal ");
		LOGGER.debug("In CookieAuthentication | In getAuthorities | Principal =" + principal );
		LOGGER.debug( "In CookieAuthentication | Finished Execution of getPrincipal" );
		
		return principal;
		
    }
	
	@Override
    public boolean isAuthenticated() {
		
		if( details == null ) {
			return true;
		}
        LOGGER.debug( "In CookieAuthentication | Starting Execution of isAuthenticated ");
		LOGGER.debug("In CookieAuthentication | In getAuthorities | Authenticated =" + authenticated );
		LOGGER.debug( "In CookieAuthentication | Finished Execution of isAuthenticated" );
		
		return authenticated;
    }

	@Override
    public void setAuthenticated(boolean authenticated) throws IllegalArgumentException {
    	LOGGER.debug( "In CookieAuthentication | Starting Execution of setAuthenticated ");
		LOGGER.debug("In CookieAuthentication | In getAuthorities | authenticated =" + authenticated );
		LOGGER.debug( "In CookieAuthentication | Finished Execution of setAuthenticated" );
		
		this.authenticated = authenticated;
    }

	
    public CookieAuthentication( UserDetails details ) {
    	this.details = ( CurrentUserDetails ) details;
    	this.principal = this.details.getId();
    }
    
    public static CookieAuthentication createBlankAuthentication() {
    	CurrentUserDetails details = new CurrentUserDetails();
    	
    	details.setName( null );
    	details.setAuthorities( new ArrayList<RoleAuthority>());
    	details.setId( null );
    	details.setUsername( null );
    	
    	return new CookieAuthentication( details );
    }
}
