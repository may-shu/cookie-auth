package com.paras.framework.security.spring;

import java.util.List;
import java.util.Map;

import org.springframework.security.core.userdetails.UserDetails;

/**
 * CurrentUserDetails holds all the information of the current user.
 * @author Paras
 *
 */
public class CurrentUserDetails implements UserDetails {

	/**
	 * 
	 */
	private static final long serialVersionUID = 8084565606054787047L;

	/**
	 * List of authorities assigned to user.
	 */
	private List<RoleAuthority> authorities;
	
	/**
	 * User-name of the current user.
	 */
	private String username;
	
	/**
	 * Id of the user.
	 */
	private String id;
	
	
	/**
	 * Password of the current user.
	 */
	private String password;
	
	/**
	 * Other details that werent adjusted in here.
	 */
	private Map<String, Object> details;
	
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	/**
	 * Name of current user.
	 */
	private String name;
	
	/**
	 * Is account expired ?
	 */
	private boolean accountExpired;
	
	/**
	 * Is account locked ?
	 */
	private boolean locked;
	
	/**
	 * Is password of user expired ?
	 */
	private boolean passwordExpired;
	
	/**
	 * Is user's account enabled ?
	 */
	private boolean active;
	
	public void setAuthorities( List<RoleAuthority> authorities ) {
		this.authorities = authorities;
	}

	@Override
	public List<RoleAuthority> getAuthorities() {
		return authorities;
	}
	
	public void setPassword( String password ) {
		this.password = password;
	}

	@Override
	public String getPassword() {
		return password;
	}
	
	public void setUsername( String userName ) {
		this.username = userName;
	}

	@Override
	public String getUsername() {
		return this.username;
	}
	
	public void setAccountNonExpired( boolean accountNonExpired ) {
		this.accountExpired = !accountNonExpired;
	}

	@Override
	public boolean isAccountNonExpired() {
		return !accountExpired;
	}
	
	public void setAccountNonLocked( boolean accountNonLocked ) {
		this.locked = !accountNonLocked;
	}

	@Override
	public boolean isAccountNonLocked() {
		return !locked;
	}
	
	public void setCredentialsNonExpired( boolean credentialsNonExpired ){ 
		this.passwordExpired = !credentialsNonExpired;
	}

	@Override
	public boolean isCredentialsNonExpired() {
		return passwordExpired;
	}
	
	public void setEnabled( boolean enabled ) {
		this.active = enabled;
	}

	@Override
	public boolean isEnabled() {
		return active;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public Map<String, Object> getDetails() {
		return details;
	}

	public void setDetails(Map<String, Object> details) {
		this.details = details;
	}
}
