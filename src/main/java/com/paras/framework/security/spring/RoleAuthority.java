package com.paras.framework.security.spring;

import org.springframework.security.core.GrantedAuthority;

/**
 * It represents a authority that can be provided to a user.
 * @author Paras
 *
 */
public class RoleAuthority implements GrantedAuthority {

	/**
	 * 
	 */
	private static final long serialVersionUID = 3530107041043659657L;
	
	/**
	 * Authority Name.
	 */
	private String authority;

	@Override
	public String getAuthority() {
		return authority;
	}

	public void setAuthority(String authority) {
		this.authority = authority;
	}
	
	public RoleAuthority(){
		
	}
	
	public RoleAuthority( String authority ) {
		this.authority = authority;
	}
}
