package com.paras.framework.security.util;

import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;

import com.paras.framework.security.spring.CookieAuthentication;
import com.paras.framework.security.spring.CurrentUserDetails;

/**
 * Session Util to extract information from session.
 * @author Paras
 *
 */
public class SessionUtil {
	
	
	/**
	 * If User is authenticated ?
	 */
	public static boolean isAuthenticated() {
		
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		CurrentUserDetails details = (CurrentUserDetails) auth.getDetails();
		
		String principal = details.getUsername();
		
		if( principal == null ) {
			return false;
		}
		
		return true;
	}

	/**
	 * Retrieve current user's id from session.
	 * @return
	 */
	public static int getCurrentUserId() {
		
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		CurrentUserDetails details = (CurrentUserDetails) auth.getDetails();
		
		return Integer.valueOf( details.getId() );
		
	}
	
	/**
	 * Retrieve current user's name from session.
	 */
	public static String getCurrentUserName() {
		
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		CurrentUserDetails details = (CurrentUserDetails) auth.getDetails();
		
		return details.getName();
	}
	
	/**
	 * Retrieve current user's email from session.
	 */
	public static String getCurrentUserEmail() {
		
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		CurrentUserDetails details = (CurrentUserDetails) auth.getDetails();
		
		return details.getUsername();
	}
	
	/**
	 * Retrieve current user's role from session.
	 */
	public static String getCurrentUserRole() {
		
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		CurrentUserDetails details = (CurrentUserDetails) auth.getDetails();
		
		return details.getAuthorities().get(0).getAuthority();
	}
	
	/**
	 * Logout a user.
	 */
	public static void logout() {
		
		Authentication auth = CookieAuthentication.createBlankAuthentication();
		
		auth.setAuthenticated( true );
		SecurityContextHolder.getContext().setAuthentication( auth );
	}
}
