package com.paras.framework.security.service;

import java.util.Collection;

import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.web.authentication.preauth.PreAuthenticatedGrantedAuthoritiesUserDetailsService;

import com.paras.framework.security.spring.CurrentUserDetails;

/**
 * Implementation of UserDetailsService
 * @author Paras
 *
 */
public class CurrentUserDetailsService extends PreAuthenticatedGrantedAuthoritiesUserDetailsService  implements UserDetailsService {
	
	@Override
    public UserDetails loadUserByUsername(String arg0) throws UsernameNotFoundException {
		CurrentUserDetails userDetails = new CurrentUserDetails();
        userDetails.setUsername(arg0);      
        return userDetails;
    }

    @Override
    protected UserDetails createuserDetails(Authentication token, Collection<? extends GrantedAuthority> authorities) {
        return super.createuserDetails(token, authorities);
    }
}
