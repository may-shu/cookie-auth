package com.paras.framework.security.handler;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.springframework.security.access.AccessDeniedException;

import com.paras.framework.security.base.Response;

public class AccessDeniedHandler implements org.springframework.security.web.access.AccessDeniedHandler {
	
	private static final String N = "N";
	private static final String MESSAGE = "You aren't authorized to perform this action.";
	
	private static final String JSON = "application/json";
	
	private static Logger LOGGER = Logger.getLogger( AccessDeniedHandler.class );

	@Override
	public void handle( HttpServletRequest request, HttpServletResponse response, AccessDeniedException ex ) throws IOException, ServletException {
		
		LOGGER.error( "In AccessDeniedHandler | Denied Access to " + request.getRequestURI() );
		
		Response res = new Response();
		
		res.setFlag( N );
		res.setMessage( MESSAGE );
		
		response.setContentType( JSON );
		response.setStatus( 401 ); response.setStatus( HttpServletResponse.SC_UNAUTHORIZED );
		
		response.getWriter().write( res.toJSON() );
		response.getWriter().flush();
		
		
		
	}

}
